jest.dontMock('../src/js/stores/AppStore');
jest.dontMock('../node_modules/eNUMERATION');
jest.dontMock('../src/js/constants/ActionConstants');
jest.dontMock('../src/js/constants/StatusConstants');

var ActionConstants = require('../src/js/constants/ActionConstants');
var StatusConstants = require('../src/js/constants/StatusConstants');

describe('AppStore', function () {
  var createBookSuccessAction = {
    type: ActionConstants.BOOK_SAVE_SUCCESS,
    data: {
      isbn: '123456789X',
      title: 'Book',
      year: 2000
    },
    result: '{ "objectId": "XXX" }'
  };

  var updateBookSuccessAction = {
    type: ActionConstants.BOOK_UPDATE_SUCCESS,
    data: {
      objectId: 'XXX',
      isbn: '123456789X',
      title: 'Book2',
      year: 2002
    }
  };

  var destroyBookSuccessAction = {
    type: ActionConstants.BOOK_DESTROY_SUCCESS,
    data: {objectId: 'XXX'}
  };

  var bookValidationErrorAction = {
    type: ActionConstants.BOOK_VALIDATION_ERROR,
    errors: {
      isbn: 'Please supply a valid ISBN in ISBN-10 format!',
      title: 'Title can\'t be an empty string and can contain maximum of 50 characters!',
      year: 'Please supply a valid year in the range from 1459 to the current one!'
    }
  };

  var nonUniqueISBNAction = {
    type: ActionConstants.NON_UNIQUE_ISBN,
    errors: {
      isbn: 'The book with such ISBN already exists!'
    }
  };

  var clearNotificationsAction = {
    type: ActionConstants.CLEAR_NOTIFICATIONS
  };

  var createBookPendingAction = {
    type: ActionConstants.REQUEST_BOOK_SAVE
  };

  var AppDispatcher;
  var AppStore;
  var callback;

  beforeEach(function() {
    AppDispatcher = require('../src/js/dispatchers/AppDispatcher');
    AppStore = require('../src/js/stores/AppStore');
    callback = AppDispatcher.register.mock.calls[0][0];
  });

  // General
  it('registers a callback with the dispatcher', function() {
    expect(AppDispatcher.register.mock.calls.length).toBe(1);
  });

  it('initializes with no books items', function() {
    var books = AppStore.getAllBooks();
    expect(books).toEqual([]);
  });

  // Get All Books SUCCESS
  it('adds all retrieved books to the `books` array', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();
    var keys = Object.keys(books);

    expect(keys.length).toBe(1);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book');
    expect(books[0].year).toEqual(2000);
  });

  // Create Book SUCCESS
  it('creates a book item', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();
    var keys = Object.keys(books);

    expect(keys.length).toBe(1);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book');
    expect(books[0].year).toEqual(2000);
  });

  // Update Book SUCCESS
  it('updates a book item', function() {
    callback(createBookSuccessAction);

    var books = AppStore.getAllBooks();

    callback(updateBookSuccessAction);
    expect(books[0].objectId).toEqual('XXX');
    expect(books[0].isbn).toEqual('123456789X');
    expect(books[0].title).toEqual('Book2');
    expect(books[0].year).toEqual(2002);
  });

  // Destroy Book SUCCESS
  it('destroys a book item', function() {
    callback(createBookSuccessAction);
    callback(destroyBookSuccessAction);
    expect(AppStore.getAllBooks().length).toBe(0);
  });

  // Create Book Validation ERROR
  it('[create book] populate `validationErrors` object with the errors if the Book data is not valid', function() {
    callback(bookValidationErrorAction);
    expect(AppStore.getNotifications().errors)
      .toEqual({
        isbn: 'Please supply a valid ISBN in ISBN-10 format!',
        title: 'Title can\'t be an empty string and can contain maximum of 50 characters!',
        year: 'Please supply a valid year in the range from 1459 to the current one!'
      });
    expect(AppStore.getNotifications().status).toBe(StatusConstants.ERROR);
  });

  // ISBN is NOT unique
  it('populate `validationErrors` object with the "ISBN is NOT unique" error if the Book with the given ISBN already exists', function() {
    callback(nonUniqueISBNAction);
    expect(AppStore.getNotifications().errors)
      .toEqual({
        isbn: 'The book with such ISBN already exists!'
      });
    expect(AppStore.getNotifications().status).toBe(StatusConstants.ERROR);
  });

  // Clear notifications
  it('clears `notifications` object on "CLEAR_NOTIFICATIONS" action', function () {
    callback(clearNotificationsAction);
    console.log('AppStore.getNotifications(): ', AppStore.getNotifications());
    expect(AppStore.getNotifications()).toEqual({
      errors: {},
      status: StatusConstants.IDLE
    });
  });

  // Default `success` notification
  it('`success` property is `idle` by default', function () {
    expect(AppStore.getNotifications().status).toEqual(StatusConstants.IDLE);
  });

  // `success` notification when success
  it('turns `status` property to `success` if the book was created successfully', function () {
    callback(createBookSuccessAction);
    expect(AppStore.getNotifications().status).toEqual(StatusConstants.SUCCESS);
  });

  it('turns `status` property to `pending` if the pending request action received', function () {
    callback(createBookPendingAction);
    expect(AppStore.getNotifications().status).toEqual(StatusConstants.PENDING);
  });
});