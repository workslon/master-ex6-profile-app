var AppDispatcher = require('../dispatchers/AppDispatcher');
var ActionConstants = require('../constants/ActionConstants');
var adapter = require('../storage-managers/ParseApiAdapter');

module.exports = {
  validate: function(modelClass, key, value) {
    var errors = {};
    errors[key] = modelClass.check(key, value).message;

    AppDispatcher.dispatch({
      type: ActionConstants.BOOK_VALIDATION_ERROR,
      errors: errors
    });
  },

  getBooks: function (modelClass) {
    var promise = adapter.retrieveAll(modelClass);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOKS,
      success: ActionConstants.REQUEST_BOOKS_SUCCESS,
      failure: ActionConstants.REQUEST_BOOKS_ERROR
    });
  },

  createBook: function (modelClass, data) {
    adapter
      .retrieve(modelClass, '', data.isbn)
      .then(function(response) {
        try {
          if (response.length) {
            AppDispatcher.dispatch({
              type: ActionConstants.NON_UNIQUE_ISBN
            });
          } else {
            data.runCloudCode = true;
            AppDispatcher.dispatchAsync(adapter.add(modelClass, data), {
              request: ActionConstants.REQUEST_BOOK_SAVE,
              success: ActionConstants.BOOK_SAVE_SUCCESS,
              failure: ActionConstants.BOOK_SAVE_ERROR
            }, data);
          }
        } catch(e) {}
      });

    AppDispatcher.dispatch({
      type: ActionConstants.REQUEST_BOOK_SAVE
    });
  },

  updateBook: function (modelClass, book, newData) {
    var promise;

    newData.runCloudCode = true;
    promise = adapter.update(modelClass, book.objectId, newData);
    newData.objectId = book.objectId;

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOK_UPDATE,
      success: ActionConstants.BOOK_UPDATE_SUCCESS,
      failure: ActionConstants.BOOK_UPDATE_ERROR
    }, newData);
  },

  deleteBook: function (modelClass, book) {
    var promise;

    book.runCloudCode = true;
    promise = adapter.destroy(modelClass, book.objectId);

    AppDispatcher.dispatchAsync(promise, {
      request: ActionConstants.REQUEST_BOOK_DESTROY,
      success: ActionConstants.BOOK_DESTROY_SUCCESS,
      failure: ActionConstants.BOOK_DESTROY_ERROR
    }, book);
  },

  clearNotifications: function () {
    AppDispatcher.dispatch({
      type: ActionConstants.CLEAR_NOTIFICATIONS
    });
  }
};