var React = require('react');
var Link = require('react-router').Link;
var AppActions = require('../actions/AppActions');
var BookModel = require('../models/Book');

module.exports = React.createClass({
  displayName: 'Book',

  _deleteBook: function () {
    AppActions.deleteBook(BookModel, this.props.book);
  },

  render: function () {
    var book = this.props.book,
        updatePath = '/update/' + book.objectId;

    return (
      <tr>
        <td scope="row">{this.props.nr}</td>
        <td>{book.isbn}</td>
        <td>{book.title}</td>
        <td>{book.year}</td>
        <td className="action-links">
          <Link className="btn btn-primary btn-xs" to={updatePath}>Update</Link>
          <a className="btn btn-danger btn-xs" onClick={this._deleteBook}>Delete</a>
        </td>
      </tr>
    );
  }
});

