var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var StatusConstants = require('../constants/StatusConstants');
var BookModel = require('../models/Book');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'CreateBook',

  componentDidUpdate: function () {
    var notifications = this.props.notifications || {};
    var status = notifications.status;

    if (status === StatusConstants.SUCCESS) {
      var refs = this.refs;
      refs.isbn.value = refs.title.value = refs.year.value = '';
    }
  },

  _createBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.createBook(BookModel, {
      isbn: isbn.value,
      title: title.value,
      year: parseInt(year.value)
    });
  },

  _validate: function(e) {
    AppActions.validate(BookModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
      <form>
        <h3>Create Book</h3>
        <div className="form-group">
          <label forHtml="isbn">ISBN</label>
          <input defaultValue="" onChange={this._validate} ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
          {errors.isbn && <span className="text-danger">{errors.isbn}</span>}
        </div>
        <div className="form-group">
          <label forHtml="title">Title</label>
          <input defaultValue="" onInput={this._validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
          {errors.title && <span className="text-danger">{errors.title}</span>}
        </div>
        <div className="form-group">
          <label forHtml="year">Year</label>
          <input defaultValue="" onInput={this._validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
          {errors.year && <span className="text-danger">{errors.year}</span>}
        </div>
        <button type="submit" onClick={this._createBook} className="btn btn-default">Submit</button>
        {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
        {status === StatusConstants.PENDING && <p className="bg-info">Creating...</p>}
        <IndexLink className="back" to="/">&laquo; back</IndexLink>
      </form>
    );
  }
});