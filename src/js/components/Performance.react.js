var React = require('react');
var Perf = require('react-addons-perf');

module.exports = React.createClass({
  displayName: 'Performance',

  _startPerf: function () {
    Perf.start();
  },

  _stopPerf: function () {
    Perf.stop();
    Perf.printWasted();
    Perf.printDOM();
  },

  render: function () {
    return (
      <div className="perf-cotrols-block well">
        <h3>Capture Performance</h3>
        <div>
          <a onClick={this._startPerf} className="btn btn-primary btn-xs" href="javascript:void(0)">Start</a>
          <a onClick={this._stopPerf} className="btn btn-warning btn-xs" href="javascript:void(0)">Stop</a>
        </div>
      </div>
    );
  }
});