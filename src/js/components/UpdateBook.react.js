var React = require('react');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore');
var StatusConstants = require('../constants/StatusConstants');
var BookModel = require('../models/Book');
var IndexLink = require('react-router').IndexLink;

module.exports = React.createClass({
  displayName: 'UpdateBook',

  componentWillMount: function() {
    this.book = AppStore.getBook(this.props.params.id);
  },

  _updateBook: function (e) {
    e.preventDefault();

    var refs = this.refs || {};
    var isbn = refs.isbn || {};
    var title = refs.title || {};
    var year = refs.year || {};

    AppActions.updateBook(BookModel, this.book, {
      title: title.value,
      year: parseInt(year.value)
    });
  },

  _validate: function(e) {
    AppActions.validate(BookModel, e.target.id, e.target.value);
  },

  render: function () {
    var notifications = this.props.notifications || {};
    var errors = notifications.errors || {};
    var status = notifications.status;

    return (
        <div>
          <h3>Update Book</h3>
          {this.book ?
            <form>
              <div className="form-group">
                <label forHtml="isbn">ISBN</label>
                <input defaultValue={this.book.isbn} disabled="disabled" ref="isbn" type="text" className="form-control" id="isbn" placeholder="ISBN" />
              </div>
              <div className="form-group">
                <label forHtml="title">Title</label>
                <input defaultValue={this.book.title} onInput={this._validate} ref="title" type="text" className="form-control" id="title" placeholder="Title" />
                {errors.title && <span className="text-danger">{errors.title}</span>}
              </div>
              <div className="form-group">
                <label forHtml="year">Year</label>
                <input defaultValue={this.book.year} onInput={this._validate} ref="year" type="text" className="form-control" id="year" placeholder="Year" />
                {errors.year && <span className="text-danger">{errors.year}</span>}
              </div>
              <button type="submit" onClick={this._updateBook} className="btn btn-default">Submit</button>
              {status === StatusConstants.SUCCESS && <p className="bg-success">Success!</p>}
              {status === StatusConstants.PENDING && <p className="bg-info">Updating...</p>}
              <IndexLink className="back" to="/">&laquo; back</IndexLink>
            </form>
          : <div>No book found...</div>}
        </div>
    );
  }
});